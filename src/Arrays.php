<?php declare(strict_types = 1);

namespace CPTeam\Strict;

class Arrays
{
	
	public function __construct()
	{
		throw new \LogicException(self::class . ' can\'t be initialized.');
	}
}
